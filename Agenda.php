<?php 
    require_once('Contacte.class.php');
    require_once('Agenda.class.php');

    $auxAgenda=new Agenda();

    $contacto1=new Contacte('Pepe',965889654);
    $contacto2=new Contacte('Juan',965847822);
    $contacto3=new Contacte('Lucia',965888842);


    $auxAgenda->añadeContacto($contacto1);
    $auxAgenda->añadeContacto($contacto2);
    $auxAgenda->añadeContacto($contacto3);


    $auxAgenda->_toString();
    $auxAgenda->borraContactoId(1001);
    $copia=clone($auxAgenda->getContactos()[1]);
    $auxAgenda->añadeContacto($copia);
    echo "</br>";
    $auxAgenda->_toString();


?>